<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Models\File;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::group(['namespace' => 'App\Http\Controllers', 'middleware' => ['auth', 'verified']], function() {

    Route::get('/dashboard', [
        'uses' => 'FileController@index',
        'as' => 'dashboard'
    ]);

    Route::post('/upload/file', [
        'uses' => 'FileController@upload',
        'as' => 'upload.file'
    ]);

    Route::post('/delete/file', [
        'uses' => 'FileController@delete',
        'as' => 'delete.file'
    ]);

    Route::post('/rename/file/{id}', [
        'uses' => 'FileController@rename',
        'as' => 'rename.file'
    ]);

    Route::get('/' . File::PATH . '{id}/{file}', [
        'uses' => 'FileController@download',
        'as' => 'download.file'
    ]);

});

require __DIR__.'/auth.php';
