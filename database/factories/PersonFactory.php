<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Person>
 */
class PersonFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'login' => substr($this->faker->userName(), 0, 10),
            'i_name' => $this->faker->firstName(),
            'f_name' => $this->faker->lastName(),
            'state' => $this->faker->numberBetween(1, 3)    
        ];
    }
}
