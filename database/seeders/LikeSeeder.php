<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Person;
use App\Models\Product;

class LikeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $persons = Person::all();

        foreach ($persons as $person) {
            $limit = rand(0, 25);

            if ($limit){
                $products = Product::inRandomOrder()
                    ->limit($limit)
                    ->pluck('id');

                $person->likes()->attach($products);
            }
        }
    }

}
