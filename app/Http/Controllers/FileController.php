<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UploadFileRequest;
use Inertia\Inertia;
use App\Models\File;


class FileController extends Controller
{
    
    public function index()
    {
        $files = Auth::user()->files;
        
        return Inertia::render('Dashboard', ['files' => $files]);
    }

    public function upload(UploadFileRequest $request)
    {   
        $sanitized = $request->sanitized();
        File::create($sanitized);

        return Redirect::route('dashboard');
    }

    public function delete(Request $request) 
    {
        $request->validate([
                    'id' => 'required|integer',
                ]);
        $file = File::find($request->id); 
        $this->authorize('action', $file);
        Storage::delete($file->path);
        File::destroy($request->id);

        return Redirect::route('dashboard');
    }

    public function rename(Request $request, int $id)
    {
        $request->validate([
                'name' => 'required|string'
            ]);

        $file = File::find($id);
        $this->authorize('action', $file);
        $file->update([
            'name' => $request->get('name') . '.' . pathinfo($file->path, PATHINFO_EXTENSION)
        ]);

        return Redirect::route('dashboard');
    }

    public function download(int $id, string $file) 
    {

        $file = File::where('path', File::PATH . $id . '/' . $file)->first();
     
        $this->authorize('action', $file);

        return Storage::download($file->path, $file->name);
    }
}
