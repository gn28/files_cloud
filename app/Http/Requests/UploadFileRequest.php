<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\File;

class UploadFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required'
        ];
    }

    public function sanitized()
    {
        $userId = Auth::user()->id;

        $data = [   
            'path' => $this->file->store('/' . File::PATH . $userId),
            'name' => $this->file->getClientOriginalName(),
            'size' => $this->file->getSize(),
            'user_id' => $userId 
        ]; 

        return $data;
    }

}
