<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RegisterRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $countCriteria = 0;

        if (preg_match('/[a-z]/', $value)) {
            $countCriteria++;            
        }
        if (preg_match('/[A-Z]/', $value)) {
            $countCriteria++;            
        }
        if (preg_match('/[0-9]/', $value)) {
            $countCriteria++;            
        }
        if (preg_match('/[@$!%*#?&]/', $value)) {
            $countCriteria++;            
        }

        if ($countCriteria >= 2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'At least two of the following criteria should to be fulfilled in the password: lower case
letters, upper case letters, numbers, special characters (@$!%*#?&).';
    }
}
