<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    public const PATH = 'storage/uploads/file/';

    protected $fillable = [
        'name',
        'size',
        'path',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class); 
    }
}
