<?php

namespace App\Policies;

use App\Models\User;
use App\Models\File;
use Illuminate\Auth\Access\HandlesAuthorization;

class FilePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     * 
     */

    public function __construct()
    {
        //
    }

    public function action(User $user, File $file)
    {
        return $user->id === $file->user_id;
    }
}
